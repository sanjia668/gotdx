module gitee.com/quant1x/gotdx

go 1.21.5

require (
	gitee.com/quant1x/gox v1.16.6
	gitee.com/quant1x/pkg v0.2.2
	golang.org/x/text v0.14.0
)

require (
	github.com/dlclark/regexp2 v1.10.0 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/google/pprof v0.0.0-20231229205709-960ae82b1e42 // indirect
	golang.org/x/exp v0.0.0-20231226003508-02704c960a9b // indirect
	golang.org/x/sys v0.15.0 // indirect
)
